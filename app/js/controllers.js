'use strict';


/* Controllers */

var staffAppControllers = angular.module('staffApp.controllers', []);


staffAppControllers.controller('TabsCtrl', ['$scope', '$location',
  function($scope, $location) {
    $scope.isActive = function(route) {
      return route === $location.path();
    };
  }]);


staffAppControllers.controller('TeamCtrl', ['$scope', 'Active', 'Team',
  function($scope, Active, Team) {
    $scope.active = Active;
    $scope.teams = Team.query();

    $scope.addTeam = function() {
      var newTeam = {
        name: $scope.newTeam
      };
      $scope.teams.push(newTeam);
      $scope.teamForm.$setPristine();
      $scope.newTeam = '';
    };

    $scope.showTeam = function() {
      _.each($scope.teams, function(team) {
        team.active = false;
      });

      var team = this.team,
          uids = [];

      team.active = true;

      _.each(team.users, function(user){
        uids.push(user.id);
      });

      $scope.active.team = team;
      $scope.active.uids = uids;
    };

  }]);


staffAppControllers.controller('SearchCtrl', ['$scope', 'Active', 'User',
  function($scope, Active, User) {
    $scope.active = Active;
    $scope.users = User.query();

    // $scope.activeUsers = [];

    // $scope.users.$promise.then(function(){
    //   var query = 'Dmitry';
    //   var filter = $filter('filter');
    //   var users = filter($scope.users, query);
    //   console.log(users);

    //   var deferred = $q.defer();
    //   deferred.resolve(users);

    //   deferred.promise.then(function(result){
    //     console.log(result);
    //   });
    // });

    // $scope.loadUsers = function(query) {
    //   var filter = $filter('filter'),
    //       orderBy = $filter('orderBy'),
    //       users = [],
    //       deferred = $q.defer();

    //   // users = filter($scope.users, query);
    //   users = orderBy(filter($scope.users, query), 'name');

    //   console.log(users);

    //   deferred.resolve(users);
    //   return deferred.promise;
    // };

    $scope.refreshTeam = function() {
      $scope.active.team.users = [];
      _.each($scope.active.uids, function(uid) {
        var user = _.findWhere($scope.users, {id: uid});
        $scope.active.team.users.push(user);
      });
    };

    // var $el = $('.user-search'),
    //     $wrap = $('.wrap', $el),
    //     $input = $('.query', $el);

    // $el.on('mouseenter', '.tm-tag', showDetails);

    // $wrap.click(function() {
    //   $input.focus();
    // });

    // function showDetails() {
    //   var $target = $(this),
    //       $remove = $('.tm-tag-remove', $target),
    //       uid = $remove.attr('tagidtoremove');

    //   uid = parseInt(uid);

    //   var user = _.findWhere($scope.users, {id: uid});

    //   $target.qtip({
    //     content: {
    //       text: user.job + ' / ' + user.grade + ' <br> Age: ' + user.age
    //     }
    //   });
    // }

  }]);


staffAppControllers.controller('ListCtrl', ['$scope', '$filter', 'Active', 'User',
  function($scope, $filter, Active, User) {
    $scope.active = Active;
    $scope.users = User.query();

    $scope.users.$promise.then(function(){
      var orderBy = $filter('orderBy');

      $scope.order = function(predicate, reverse) {
        $scope.users = orderBy($scope.users, predicate, reverse);
      };

      $scope.order('name', false);
    });

    $scope.joinTeam = function(e) {
      e.stopPropagation();

      var team = $scope.active.team;

      if (!team) {
        return;
      }

      var user = this.user,
          isInTeam = _.contains(team.users, user);

      if (isInTeam) {
        return;
      }

      team.users = team.users || [];
      team.users.push(user);
    };

    $scope.showReviews = function() {
      _.each($scope.users, function(user) {
        user.active = false;
      });

      var user = this.user;
      user.active = true;

      $scope.active.user = user;
    };

    $scope.addReview = function() {
      var user = this.user;
      user.reviews = user.reviews || [];
      user.reviews.push(user.newReview);
      user.reviewForm.$setPristine();
      user.newReview = '';
    };

  }]);
