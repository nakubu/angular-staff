'use strict';

/* Services */

var staffAppServices = angular.module('staffApp.services', ['ngResource']);

staffAppServices.value('version', '0.1');

staffAppServices.factory('Active',
  function() {
    return {
      team: null,
      user: null,
      uids: []
    };
  })

staffAppServices.factory('Team', ['$resource',
  function($resource){
    return $resource('staff/teams.json', {}, {
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);

staffAppServices.factory('User', ['$resource',
  function($resource){
    return $resource('staff/users.json', {}, {
      query: {method:'GET', params:{}, isArray:true}
    });
  }]);
