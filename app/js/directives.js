'use strict';

/* Directives */


var staffAppDirectives = angular.module('staffApp.directives', []);

staffAppDirectives.directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);



staffAppDirectives.directive('userSearch', ['$compile', function($compile) {
    return {
      link: function (scope, element) {
        var input = element.find('input.query');

        scope.users.$promise.then(init);

        function init() {
          scope.$watch('active.team.id', update, true);

          input.on('tm:pushed tm:popped tm:spliced', function() {
            var uids = input.data().tlid;
            console.log(arguments);

            scope.$apply(function(){
              scope.active.uids = uids;
            });

            var tags = element.find('.tm-tag');

            angular.forEach(tags, function(tag) {
              console.log(tag);
            });

            tags.attr('tooltip-html-unsafe', '{{user.job}} / {{user.grade}} <br> Age: {{user.age}}');

            $compile(tags)(scope);

          }).on('tm:splicing', function() {
            // $('.qtip').qtip('hide');
          });
        }

        function update() {
          if (!scope.active.team) {
            return;
          }

          input.val('').tagsManager('empty');

          _.each(scope.active.team.users, function(user){
            input.tagsManager('pushTag', user.name, true, user.id);
          });
        }
      }
    };
  }]);



staffAppDirectives.directive('tagsManager', [function() {
    return {
      link: function (scope, element) {
        var input = element;

        scope.users.$promise.then(init);

        function init() {
          initTagManager();
          initTypeahead();
        }

        function initTagManager() {
          input.tagsManager({
            externalTagId: true,
            blinkBGColor_1: '#f99',
            blinkBGColor_2: '#f5f5f5',
            validator: function(tag, id) {
              return !!id;
            }
          });
        }

        function initTypeahead() {
          var userSource = new Bloodhound({
            local: scope.users,
            limit: 10,
            datumTokenizer: function(d) {
              var name = Bloodhound.tokenizers.whitespace(d.name),
                  job = Bloodhound.tokenizers.whitespace(d.job),
                  grade = Bloodhound.tokenizers.whitespace(d.grade),
                  tokens = name.concat(job, grade);
              return tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace
          });

          userSource.initialize();

          input.typeahead({
            hint: false,
            highlight: true
          }, {
            name: 'users',
            source: userSource.ttAdapter(),
            displayKey: 'name',
            templates: {
              suggestion: function(d) {
                var label = d.name + ': ' + d.job + ' / ' + d.grade;
                return label;
              }
            }
          }).on('typeahead:selected', function (e, d) {
            input.tagsManager('pushTag', d.name, false, d.id);
          }).on('blur', function(e){
            input.val('');
          });

        }
      }
    };
  }]);
