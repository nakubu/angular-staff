'use strict';


// Declare app level module which depends on filters, and services
var staffApp = angular.module('staffApp', [
  'ngRoute',
  'ui.bootstrap',
  'staffApp.filters',
  'staffApp.services',
  'staffApp.directives',
  'staffApp.controllers'
]);

staffApp.config(['$routeProvider', '$tooltipProvider', function($routeProvider, $tooltipProvider) {
  $routeProvider.when('/search', {templateUrl: 'partials/search.html', controller: 'SearchCtrl'});
  $routeProvider.when('/list', {templateUrl: 'partials/list.html', controller: 'ListCtrl'});
  $routeProvider.otherwise({redirectTo: '/search'});

var arr = [1,2,3,4,5,6,7,8,9];

for (var i = arr.length; --i;) {
  console.log(i);
}


  $tooltipProvider.options({
    html: true,
    placement: 'bottom',
    trigger: 'mouseenter'
  });
}]);
