'use strict';


var qtipOpts = {
  overwrite: false,
  show: {
      ready: true
  },
  position: {
    my: 'top left',
    at: 'bottom left'
  },
  style: {
    classes: 'qtip-bootstrap'
  }
};

$.extend(true, $.fn.qtip.defaults, qtipOpts);
